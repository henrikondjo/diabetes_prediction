# Import libraries
import warnings
warnings.filterwarnings('ignore')
import streamlit as st
from pandas import read_csv
import pandas as pd
# Modules d`apprentissage automatique
from sklearn.model_selection import train_test_split # Permet de separer les donnees en train et test
# Algorithme d`apprentissage (classification, model)
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
#metric de la classification
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score
 # Permet de separer les donnees en train et test
from sklearn.model_selection import KFold, cross_val_score
# Algorithme d`apprentissage (Regression, model)
from sklearn.linear_model import LinearRegression, Ridge, Lasso, ElasticNet
from sklearn.neighbors import KNeighborsRegressor
from sklearn.tree import DecisionTreeRegressor


##############################Fonction pour la prédiction de classification##########################################################################################
# Fonction pour la prédiction de classification
def Classification():

    st.write('''
            # Bienvenue dans l'application de Prediction de Diabete
            Cette Application classifie le diabete en Positif ou Negatif, Application simple 
            à utilisé.
            
            ''')
     # Charger le jeu de données "pima-indians-diabetes"
    filename = './datasets/pima-indians-diabetes.data.csv'
    colnames = ['preg', 'plas', 'pres', 'skin', 'test', 'mas', 'pedi', 'age', 'class']
    data = read_csv(filename, names=colnames)
    classes = ['Negative-Diabetes', 'Positive-Diabetes']
    # Tranformer dataframe en matrice de valeurs. On enleve les labels
    array = data.values
    # On separe les inputs (X) et output(Y)
    X =  array[  :  , 0: -1] # Tout sauf la derniere colonne
    Y = array[ : ,  -1]
    proportion_test = 0.30
    seed = 8 # valeur qui permet la reproduction de resultat
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=proportion_test,  random_state = seed)                                      
    # Aprrentissage
    models = {
        "Régression logistique": LogisticRegression(),
        "Forêt d'arbres décisionnels": RandomForestClassifier(),
        "classification de decision": DecisionTreeClassifier()
    }
    st.title("Choix d'un modèle ")
    choixmodel = st.selectbox("selectionez un modèle", list(models.keys()))
    model = models[ choixmodel] # Objet de type  model
    model.fit(X_train, Y_train)
    prediction = model.predict(X_test)
    #liste de metric
    metric = ["Accuracy", "Recall", "Precision", "F1-Score"]
    # Radiobox pour choisir la métrique
    choixmet = st.sidebar.radio("Sélectionnez une métrique de la classification", metric)
    # Calcul et affichage de la métrique sélectionnée
    st.write('Metric avec Sklearn\n--------------------')
    if choixmet == "Accuracy":
        val = accuracy_score(Y_test, prediction)
    elif choixmet == "Recall":
        val= recall_score(Y_test, prediction)
    elif choixmet == "Precision":
        val= precision_score(Y_test, prediction)
    else:
        val= f1_score(Y_test, prediction)
    st.write(f'{choixmet}: {val.round(2)*100.0}%')
    # Sidebar pour entrer les valeurs en temps-reel
    st.sidebar.header('veuillez entrer vos différentes données')
    df = user_input_parameters()
    #st.write(df)
    # Prediction 
    user_input = df
    predict_patient = model.predict(user_input)
    proba_patient = model.predict_proba(user_input)
    st.subheader('Prediction')
    val=(classes[int(predict_patient)])
    if(val=='Negative-Diabetes'):
        st.write(val)
    else:
        st.write(f" <font color='red'>{val}</font>", unsafe_allow_html=True)
    st.subheader('Probabilité')
    st.write(proba_patient)
# Creer une fonction pour collecter des donnees
def user_input_parameters():
    preg = st.sidebar.slider('Preg', 0.0, 17.0, 3.8)
    plas = st.sidebar.slider('Plas', 0.0, 199.0, 120.8)
    pres = st.sidebar.slider('Pres', 0.0, 122.0, 69.1)
    skin = st.sidebar.slider('Skin', 0.0, 99.0, 20.5)
    test = st.sidebar.slider('Test', 0.0, 846.0, 79.7)
    mas = st.sidebar.slider('Mas', 0.0, 67.1, 31.9)
    pedi = st.sidebar.slider('Pedi', 0.1, 2.4, 0.4)
    age = st.sidebar.slider('Age', 21.0, 81.0, 33.0)
    data = {
        'preg': preg,
        'plas': plas,
        'pres': pres,
        'skin': skin,
        'test': test,
        'mas': mas,
        'pedi': pedi,
        'age': age
    }
    dataframe = pd.DataFrame(data, index=[0])
    return dataframe    

#################################### Fonction pour la prédiction de régression##############################################################################
# Fonction pour la prédiction de régression
def Regression():
    st.write('''
            # Bienvenue dans l'application de Prediction des Maisons
             Application simple 
            à utilisé.
            
            ''')
    filename = './datasets/housing.csv'
    colnames = ['CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE', 'DIS', 'RAD', 'TAX','PTRATIO', 'B', 'LSTAT', 'MEDV']
    data = read_csv(filename, names=colnames, delim_whitespace=True)
    # Tranformer dataframe en matrice de valeurs. On enleve les labels
    array = data.values
    # On separe les inputs (X) et output(Y)
    X =  array[  :  , 0: -1] # Tout sauf la derniere colonne
    Y = array[ : ,  -1]
    folds = 5
    seed = 10
    models = {
    'Linear Regression': LinearRegression(),
    'Ridge': Ridge(),
    'Lasso': Lasso(),
    'ElasticNet': ElasticNet(),
    'K-NN': KNeighborsRegressor(),
    'Decision Tree': DecisionTreeRegressor()
    }
    st.title("Choix d'un modèle ")
    choixmodel = st.selectbox("selectionez un modèle", list(models.keys()))
    model = models[choixmodel]
        # Métriques de régression disponibles
    regression_metrics = [
        "neg_mean_squared_error" ,
        "neg_mean_absolute_error",
        "r2"
    ]
    # Radiobox pour choisir la métrique
    choixmet = st.sidebar.radio("Sélectionnez une métrique de la Regression",regression_metrics)
    if choixmet == "neg_mean_squared_error":
        val ="neg_mean_squared_error"      
    elif choixmet == "neg_mean_absolute_error":
        val= "neg_mean_absolute_error"
    elif choixmet == "r2":
        val= "r2"
    kfold = KFold(n_splits=5, shuffle=True, random_state = seed)
    results = cross_val_score(model, X, Y, cv=kfold, scoring=val)
    st.write(f'{val}:  {(results.mean().round(2))} ({(results.std().round(2))})')
######################################Fonction principale de l'application#####################################################################
# Fonction principale de l'application
def prediction_app():
    st.title("Application de Prédiction")

    # Choix du type de prédiction (classification ou régression)
    prediction_type = st.sidebar.radio("Choisissez le type de prédiction", ["Classification", "Régression"])

    if prediction_type == "Classification":
        Classification()
    else:
        Regression()
####################################Exécution de l'application Streamlit################################################################
# Exécution de l'application Streamlit
if __name__ == "__main__":
    prediction_app()
